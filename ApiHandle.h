// ApiHandle.h

#ifndef _APIHANDLE_h
#define _APIHANDLE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include <stdint.h>
#include <ArduinoJson.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <sha256.h>

class ApiHandle {
public:
	ApiHandle(ESP8266WebServer *web_server);

	String getContentType(String file_name);
	
	void login();
	void logout();
	void changePassword();

	void changeControlItemStatus();
	void queryControlItemStatus();

	void changePermissionLevel();
	void queryPermissionLevel();

	void queryAccount();
	void queryAllAccount();
	void newAccount();

private:
	ESP8266WebServer *web_server = nullptr;

	bool verifyIdAndKey(String user_id, String key);
	bool verifyIdAndPassword(String user_id, String password);

	char *generateLoginKey(String user_id);
	JsonObject *openJsonFile(String file_path);

};
#endif

