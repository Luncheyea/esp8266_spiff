#include "ApiHandle.h"

ApiHandle::ApiHandle(ESP8266WebServer *web_server) {
	this->web_server = web_server;
}

String ApiHandle::getContentType(String file_name) {
	if (web_server->hasArg("download")) return "application/octet-stream";
	else if (file_name.endsWith(".htm")) return "text/html";
	else if (file_name.endsWith(".html")) return "text/html";
	else if (file_name.endsWith(".css")) return "text/css";
	else if (file_name.endsWith(".js")) return "application/javascript";
	else if (file_name.endsWith(".png")) return "image/png";
	else if (file_name.endsWith(".gif")) return "image/gif";
	else if (file_name.endsWith(".jpg")) return "image/jpeg";
	else if (file_name.endsWith(".ico")) return "image/x-icon";
	else if (file_name.endsWith(".xml")) return "text/xml";
	else if (file_name.endsWith(".pdf")) return "application/x-pdf";
	else if (file_name.endsWith(".zip")) return "application/x-zip";
	else if (file_name.endsWith(".gz")) return "application/x-gzip";
	return "text/plain";
}

void ApiHandle::login() {
	if (!(this->web_server->hasArg("user_id") && this->web_server->hasArg("password"))) {
		return;
	}

	bool is_valid = verifyUserIdAndPassword(this->web_server->arg("user_id"), this->web_server->arg("password"));
	Serial.println("user_id : " + this->web_server->arg("user_id") + ", password : " + this->web_server->arg("password"));

	if (is_valid) {
		Serial.println(F("logon successful!"));
		char *login_hash = generateLoginHash(this->web_server->arg("user_id"));
		//web_server.send(200, "text/plain", String(login_hash));
		createLoginJosn(this->web_server->arg("user_id"), login_hash);

		File file = SPIFFS.open("/data/login/" + this->web_server->arg("user_id") + ".txt", "r");
		this->web_server->streamFile(file, "text/plain");
		file.close();
	}
	else {
		Serial.println(F("logon failed!"));
		this->web_server->send(200, "text/plain", "{ \"login_status\" : false }");
	}
}

void ApiHandle::logout() {

}

void ApiHandle::changePassword() {

}

void ApiHandle::changeControlItemStatus() {

}

void ApiHandle::queryControlItemStatus() {

}

void ApiHandle::changePermissionLevel() {

}

void ApiHandle::queryPermissionLevel() {

}

void ApiHandle::queryAccount() {

}

void ApiHandle::queryAllAccount() {

}

void ApiHandle::newAccount() {

}

bool ApiHandle::verifyIdAndKey(String user_id, String key) {
	JsonObject *json = openJsonFile("/data/user/" + user_id + ".txt");

	if (json == NULL) {
		return false;
	}

	const char *js_user_id = (*json)["user_id"];
	const char *js_login_hash = (*json)["login_hash"];

	Serial.print(F("js_user_id : "));
	Serial.print(js_user_id);
	Serial.print(F(", js_login_hash : "));
	Serial.println(js_login_hash);
	if (user_id.equals(js_user_id) && key.equals(js_login_hash)) {
		return true;
	}

	return false;
}

bool ApiHandle::verifyIdAndPassword(String user_id, String password) {
	JsonObject *json = openJsonFile("/data/user/" + user_id + ".txt");

	if (json == NULL) {
		return false;
	}

	const char *js_user_id = (*json)["user_id"];
	const char *js_password = (*json)["password"];
	const char *js_login_hash = (*json)["login_hash"];

	Serial.print(F("js_user_id : "));
	Serial.print(js_user_id);
	Serial.print(F(", js_password : "));
	Serial.println(js_password);
	if (user_id.equals(js_user_id) && password.equals(js_password)) {
		return true;
	}

	return false;
}

char *ApiHandle::generateLoginKey(String user_id) {
	JsonObject *json = openJsonFile("/data/user/" + user_id + ".txt");

	if (json == NULL) {
		return NULL;
	}
	const char *ori_user_id = (*json)["user_id"];
	const char *ori_password = (*json)["password"];
	const uint8_t ori_permission_level = (*json)["permission_level"];

	randomSeed(millis());
	uint8_t base = (uint8_t)random(0, 255);
	uint32_t str_len = user_id.length() + 1;
	uint8_t *msg = (uint8_t *)calloc(str_len, sizeof(uint8_t));
	user_id.getBytes(msg, str_len);

	for (uint8_t i = 0; i < str_len; ++i) {
		msg[i] = (uint8_t)(((uint16_t)msg[i] * base) % 256);
	}

	uint8_t hash[SHA256_BLOCK_SIZE];
	static char login_hash[2 * SHA256_BLOCK_SIZE + 2];

	Sha256 *sha256Instance = new Sha256();
	sha256Instance->update(msg, str_len);
	sha256Instance->final(hash);

	for (uint8_t i = 0; i < SHA256_BLOCK_SIZE; ++i)
		sprintf(login_hash + 2 * i, "%02X", hash[i]);

	Serial.print("HSA256 : ");
	Serial.println(login_hash);

	delete sha256Instance;
	free(msg);

	StaticJsonBuffer<1024> jsonBuffer;
	JsonObject &new_json = jsonBuffer.createObject();
	new_json["user_id"] = String(ori_user_id);
	new_json["password"] = String(ori_password);
	new_json["permission_level"] = ori_permission_level;
	new_json["login_hash"] = login_hash;

	File file = SPIFFS.open("/data/user/" + user_id + ".txt", "w");

	if (new_json.printTo(file) == 0) {
		Serial.println(F("Failed to write to file"));
	}
	file.close();

	return login_hash;
}

JsonObject *ApiHandle::openJsonFile(String file_path) {
	if (!SPIFFS.exists(file_path)) {
		Serial.println("error open : " + file_path);
		return nullptr;
	}

	File file = SPIFFS.open(file_path, "r");

	StaticJsonBuffer<1024> jsonBuffer;
	JsonObject &json = jsonBuffer.parseObject(file);
	file.close();

	if (!json.success()) {
		Serial.println(F("Failed to parse config file"));
		return nullptr;
	}

	return &json;
}

