#include "ApiHandle.h"
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <sha256.h>

const char *wifi_ssid = "HTC Portable Hotspot CA84"; //"dlink_E541A";
const char *wifi_password = "72f800efe130"; //"ipae541a";
const char *host = "e541a";

ESP8266WebServer web_server(80);

String getContentType(String file_name) {
	if (web_server.hasArg("download")) return "application/octet-stream";
	else if (file_name.endsWith(".htm")) return "text/html";
	else if (file_name.endsWith(".html")) return "text/html";
	else if (file_name.endsWith(".css")) return "text/css";
	else if (file_name.endsWith(".js")) return "application/javascript";
	else if (file_name.endsWith(".png")) return "image/png";
	else if (file_name.endsWith(".gif")) return "image/gif";
	else if (file_name.endsWith(".jpg")) return "image/jpeg";
	else if (file_name.endsWith(".ico")) return "image/x-icon";
	else if (file_name.endsWith(".xml")) return "text/xml";
	else if (file_name.endsWith(".pdf")) return "application/x-pdf";
	else if (file_name.endsWith(".zip")) return "application/x-zip";
	else if (file_name.endsWith(".gz")) return "application/x-gzip";
	return "text/plain";
}

JsonObject *openJsonFile(String file_path) {
  if (!SPIFFS.exists(file_path)) {
    Serial.println("error open : " + file_path);
    return NULL;
  }

  File file = SPIFFS.open(file_path, "r");

  StaticJsonBuffer<1024> jsonBuffer;
  JsonObject &json = jsonBuffer.parseObject(file);
  file.close();

  if (!json.success()) {
    Serial.println("Failed to parse config file");
    return NULL;
  }

  return &json;
}

bool verifyUserIdAndPassword(String user_id, String password) {
  JsonObject *json = openJsonFile("/data/user/" + user_id + ".txt");

  if (json == NULL) {
    return false;
  }

  const char *js_user_id = (*json)["user_id"];
  const char *js_password = (*json)["password"];
  const char *js_login_hash = (*json)["login_hash"];

  Serial.print("js_user_id : ");
  Serial.print(js_user_id);
  Serial.print(", js_password : ");
  Serial.println(js_password);
  if (user_id.equals(js_user_id) && password.equals(js_password)) {
    return true;
  }

  return false;
}

bool verifyUserIdAndLoginHash(String user_id, String login_hash) {
  JsonObject *json = openJsonFile("/data/user/" + user_id + ".txt");

  if (json == NULL) {
    return false;
  }

  const char *js_user_id = (*json)["user_id"];
  const char *js_login_hash = (*json)["login_hash"];

  Serial.print("js_user_id : ");
  Serial.print(js_user_id);
  Serial.print(", js_login_hash : ");
  Serial.println(js_login_hash);
  if (user_id.equals(js_user_id) && login_hash.equals(js_login_hash)) {
    return true;
  }

  return false;
}

void getAdminLevel(String user_id) {
  JsonObject *json = openJsonFile("/data/user/" + user_id + ".txt");

  if (json == NULL) {
    return;
  }

  web_server.send(200, "text/plain", (*json)["permission_level"]);
}

void setAdminLevel(String user_id, String login_hash, uint8_t permission_level) {
  JsonObject *json = openJsonFile("/data/user/" + user_id + ".txt");

  if (json == NULL) {
    return;
  }

  const char *js_user_id = (*json)["user_id"];
  const char *js_login_hash = (*json)["login_hash"];
  if (user_id.equals(js_user_id) && login_hash.equals(js_login_hash)) {
    //SPIFFS.remove("/data/user/" + user_id + ".txt");
    File file = SPIFFS.open("/data/user/" + user_id + ".txt", "w");

    (*json)["permission_level"] = permission_level;

    if (json->printTo(file) == 0) {
      Serial.println(F("Failed to write to file"));
    }
    file.close();
  }
}

char *generateLoginHash(String user_id) {
  JsonObject *json = openJsonFile("/data/user/" + user_id + ".txt");

  if (json == NULL) {
    return NULL;
  }
  const char *ori_user_id = (*json)["user_id"];
  const char *ori_password = (*json)["password"];
  const uint8_t ori_permission_level = (*json)["permission_level"];

  randomSeed(millis());
  uint8_t base = (uint8_t)random(0, 255);
  uint32_t str_len = user_id.length() + 1;
  uint8_t msg[str_len];
  user_id.getBytes(msg, str_len);

  for (uint8_t i = 0; i < str_len; ++i) {
    msg[i] = (uint8_t)(((uint16_t)msg[i] * base) % 256);
  }

  uint8_t hash[SHA256_BLOCK_SIZE];
  static char login_hash[2 * SHA256_BLOCK_SIZE + 2];

  Sha256 *sha256Instance = new Sha256();
  sha256Instance->update(msg, str_len);
  sha256Instance->final(hash);

  for (uint8_t i = 0; i < SHA256_BLOCK_SIZE; ++i)
    sprintf(login_hash + 2 * i, "%02X", hash[i]);

  Serial.print("HSA256 : ");
  Serial.println(login_hash);

  delete sha256Instance;


  StaticJsonBuffer<1024> jsonBuffer;
  JsonObject &new_json = jsonBuffer.createObject();
  new_json["user_id"] = String(ori_user_id);
  new_json["password"] = String(ori_password);
  new_json["permission_level"] = ori_permission_level;
  new_json["login_hash"] = login_hash;

  File file = SPIFFS.open("/data/user/" + user_id + ".txt", "w");

  //(*json)["login_hash"] = login_hash;

  if (new_json.printTo(file) == 0) {
    Serial.println(F("Failed to write to file"));
  }
  file.close();

  return login_hash;
}

void createLoginJosn(String user_id, char *login_hash) {
  JsonObject *json = openJsonFile("/data/user/" + user_id + ".txt");

  if (json == NULL) {
    return;
  }
  const char *ori_user_id = (*json)["user_id"];
  const uint8_t ori_permission_level = (*json)["permission_level"];

  StaticJsonBuffer<1024> jsonBuffer;
  JsonObject &new_json = jsonBuffer.createObject();
  new_json["user_id"] = String(ori_user_id);
  new_json["permission_level"] = ori_permission_level;
  new_json["login_status"] = true;
  new_json["login_hash"] = login_hash;

  File file = SPIFFS.open("/data/login/" + user_id + ".txt", "w");

  if (new_json.printTo(file) == 0) {
    Serial.println(F("Failed to write to file"));
  }
  file.close();
}

void createContorlStatusJson() {
  StaticJsonBuffer<1024> jsonBuffer;
  JsonObject &new_json = jsonBuffer.createObject();
  new_json["led_status"] = digitalRead(LED_BUILTIN);

  File file = SPIFFS.open("/data/login/status.txt", "w");
  if (new_json.printTo(file) == 0) {
    Serial.println(F("Failed to write to file"));
  }
  file.close();
}

void handleFileRead(String path) {
  Serial.println("handleFileRead: " + path); // 在序列埠顯示路徑

  if (path.equals("/")) {
    path += "web/index.htm";
  }

  String content_type = getContentType(path);

  if (!SPIFFS.exists(path)) {
    path = "/web/error.htm";
    content_type = "text/html";
  }
  else {
    if (web_server.hasArg("user_id") && web_server.hasArg("password")) {
      bool is_valid = verifyUserIdAndPassword(web_server.arg("user_id"), web_server.arg("password"));
      Serial.println("user_id : " + web_server.arg("user_id") + ", password : " + web_server.arg("password"));
      if (is_valid) {
        Serial.println("logon successful!");
        char *login_hash = generateLoginHash(web_server.arg("user_id"));
        //web_server.send(200, "text/plain", String(login_hash));
        createLoginJosn(web_server.arg("user_id"), login_hash);
        ////////////////////////
        File file = SPIFFS.open("/data/login/" + web_server.arg("user_id") + ".txt", "r");
        web_server.streamFile(file, "text/plain");
        file.close();
      }
      else {
        Serial.println("logon failed!");
        web_server.send(200, "text/plain", "{ \"login_status\" : false }");
      }
    }

    if (web_server.hasArg("user_id") && web_server.hasArg("login_hash")) {
      bool is_login = verifyUserIdAndLoginHash(web_server.arg("user_id"), web_server.arg("login_hash"));
      //Serial.println("user_id : " + web_server.arg("user_id") + ", login_hash : " + web_server.arg("login_hash"));
      if (is_login) {
        if (web_server.hasArg("permission_level")) {
          getAdminLevel(web_server.arg("user_id"));
        }
        else if (web_server.hasArg("led_status")) {
          //bool led_status = !web_server.arg("led_status").equals("Inversion");
          digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
          Serial.print("led_status : ");
          Serial.println(digitalRead(LED_BUILTIN));
        }
        else if (web_server.hasArg("control_status")) {
          createContorlStatusJson();
          //web_server.send(200, "text/plain", "{\"led_status\":" + String() + "}");
          File file = SPIFFS.open("/data/login/status.txt", "r");
          web_server.streamFile(file, "text/plain");
          file.close();

        }
        else if (web_server.hasArg("logout")) {
          Serial.print(web_server.arg("user_id"));
          Serial.println(" is logout!");

          generateLoginHash(web_server.arg("user_id"));
        }
        else {
          web_server.send(200, "text/plain", "{\"error\":-1}");
        }

      }
      else {
        web_server.send(200, "text/plain", "{\"error\":-1}");
      }
    }
  }

  File file = SPIFFS.open(path, "r");
  web_server.streamFile(file, content_type);
  file.close();
}

void apiHandle() {
  
}

void setup() {
  Serial.begin(115200);

  SPIFFS.begin();

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(F("."));
  }
  Serial.println("");
  Serial.print(F("Connected to "));
  Serial.println(wifi_ssid);
  Serial.print(F("IP address: "));
  Serial.println(WiFi.localIP());
  IPAddress ip = WiFi.localIP();

  if (!MDNS.begin(host, ip)) {
    Serial.println(F("Error setting up MDNS responder!"));
    while (1) {
      delay(1000);
    }
  }
  Serial.println(F("mDNS responder started"));

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(F("."));
  }

  Serial.println("");
  Serial.print(F("Connected! IP address: "));
  Serial.println(WiFi.localIP());


  web_server.on("/api/request/login", apiHandleLogin);

  web_server.onNotFound([]() {
    handleFileRead(web_server.uri());
  });

  web_server.begin();
  Serial.println(F("HTTP server started"));

  MDNS.setInstanceName(F("Cubie's ESP8266"));
  MDNS.addService("http", "tcp", 80);

  //////////////////
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
}

void loop() {
  web_server.handleClient();
}





